import React from 'react';

 function Rtif({boolean, ...props}) {
    const { children } = props;
    
    if (boolean)
        return (
                {...children}
        );
    return null;
}
export default Rtif;
