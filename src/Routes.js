import React from "react";
import { Route, Switch, Redirect, Link } from "react-router-dom";
import { MDBCard } from 'mdbreact';
import { createBrowserHistory } from 'history';
import Time from './Components/TimeComponent/Time';
import Timer from './Components/TimerComponent/Timer'
import StopWatch from './Components/StopwatchComponent/Stopwatch'


class Routes extends React.Component {
    constructor(props) {
        super(props)
        this.state = { user: '' }
    }
    history = createBrowserHistory();

    render() {
        return (
            <Switch>
                <Route exact path='/time' component={() => {
                    return <Time />
                    //   return <Redirect to="/dashboard"></Redirect>
                }} />

                <Route exact path='/' component={() => {
                    return <Time />
                    //   return <Redirect to="/dashboard"></Redirect>
                }} />

                <Route exact path='/timer' component={() => {
                    return <Timer />
                    //   return <Redirect to="/dashboard"></Redirect>
                }} />

                <Route exact path='/stopwatch' component={() => {
                    // return <Redirect to="/dashboard"></Redirect>
                    return <StopWatch></StopWatch>
                }} />

                <Route path="*" component={() => {
                    return (
                        <MDBCard>
                            <h2 className="text-center"> 404 NOT FOUND</h2>
                        </MDBCard>
                    )
                }
                } />
            </Switch>
        );
    }

}
export default Routes;
