import React from 'react';
import './Timer.css'

class Timer extends React.Component {

    constructor() {
        super()
        this.state = {
            hours: 0, minutes: 0, seconds: 0
        }
    }

    checkTime(i) {
        if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
        return i;
    }

    timer(i) {
        let j = i++;
        j++;
        this.setState({ seconds: j++ })

        if (this.state.seconds > 59) {
            this.state.seconds = 0;
            this.setState({ minutes: this.state.minutes + 1 })
        }
        if (this.state.minutes > 59) {
            this.state.minutes = 0;
            this.setState({ hours: this.state.hours + 1 })
        }
    }


    componentDidMount() {
        setInterval(() => {
            this.timer(this.state.seconds)
        }, 1000);
    }

    render() {
        return (
            <div className="jumbtrons timer">

                <div className="d-flex justify-content-center ">
                    <div >
                        <h6>Timer</h6>
                    </div>
                  
                </div>
                <div className="d-flex justify-content-center">
                    <h1>{this.checkTime(this.state.hours)}:{this.checkTime(this.state.minutes)}:{this.checkTime(this.state.seconds)}</h1>
                </div>


            </div>
        )
    }
}
export default Timer;