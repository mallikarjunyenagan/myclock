import React from 'react';
import './Time.css';
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';



class Time extends React.Component {
    constructor() {
        super()
        this.state = {
            time: '',
            date: '',
            intervel: '', modal: false,
            alarmhours: '',
            alarmminutes: '',
            alarmmessage: '',alarmModal:false

        }
    }


    startTime() {
        // var today = new Date();
        // let hour = today.getHours();
        // let min = today.getMinutes();
        // let sec = today.getSeconds();
        // sec = this.checkTime(sec)
        // min = this.checkTime(min)
        // let time = hour + ':' + min + ':' + sec


        // let timeString = time
        let timeString12hr = new Date()


        const date = new Date();
        const formattedDate = date.toLocaleDateString('en-GB', {
            weekday: 'long', day: 'numeric', month: 'short', year: 'numeric'
        }).replace(/ /g, ' ');

        //    console.log(timeString12hr)
        this.setState({ time: timeString12hr.toLocaleTimeString('en-US', { hour: 'numeric', hour12: true, minute: 'numeric', second: 'numeric' }), date: formattedDate })

        // console.log(this.state.time)
        // return time;

    }
    checkTime(i) {
        if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
        return i;
    }

    handleInputChange = (e) => {
        console.log(this.state.alarmhours, this.state.alarmminutes)
        this.setState({

            [e.target.name]: e.target.value

        })
        console.log(this.state.alarmhours, this.state.alarmminutes)
    }

    componentDidMount() {
        this.setState({ intervel: setInterval(() => { this.startTime() }, 1000) })

        // var event = new Date();
        // console.log(event, 'actual')
        // let ssd = event.toLocaleString('en-GB', { timeZone: 'Europe/London' }, {
        //     weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'
        // })
        // // let sst = event.toTimeString('en-GB', { timeZone: 'Asia/Kolkata' })
        // console.log(ssd, "string");
        // var dd = new Date(ssd)


    }
    alarm = () => {
        let secs = this.state.alarmhours * 60 * 60 + this.state.alarmminutes * 60
        console.log(secs, this.state.alarmhours, this.state.alarmminutes)
        let msg = this.state.alarmmessage
        setTimeout(() => {

            this.state.alarmModal=true

        }, secs * 1000)
        this.state.modal = false

    }
    componentWillUnmount() {
        clearInterval(this.state.intervel)
    }

    toggle = () => {
        this.setState({
            modal: !this.state.modal
        });
        if (!this.state.modal) {
            this.state.alarmhours = '';
            this.state.alarmminutes = '';
            this.state.alarmmessage = ''
        }
    }
    alarmtoggle=()=>{
        this.setState({alarmModal:!this.state.alarmModal})
    }

    render() {

        return (
            <div>
                <div className="d-flex justify-content-center">
                    <div className="localTime">
                        <div >
                            <div >
                                <h6>Time Now</h6>
                            </div>
                            <h1>{this.state.time}</h1>
                            <div>
                                <h5>{this.state.date}</h5>
                            </div>
                            <div >
                                <MDBBtn onClick={this.toggle} size="sm">Set Alarm</MDBBtn>
                            </div>
                            <MDBContainer>

                                <MDBModal isOpen={this.state.modal} toggle={this.toggle}>
                                    <MDBModalHeader toggle={this.toggle}>Alarm</MDBModalHeader>
                                    <MDBModalBody >
                                        <div className=" row">
                                            <div className="col">
                                                <input className="form-control form-control-md" min="0" placeholder="hours" pattern="[0-9.]+" max='12' value={this.state.alarmhours}
                                                    onChange={this.handleInputChange} name="alarmhours" type="number" />
                                            </div>
                                            <div className="col">
                                                <input className="form-control form-control-md" min="0" placeholder="minutes" pattern="[0-9.]+" max='59' name="alarmminutes" value={this.state.alarmminutes}
                                                    onChange={this.handleInputChange} maxLength="2" type="number" />
                                            </div>
                                        </div>
                                        <br />
                                        <div className="row">
                                            <div className="col">
                                                <input className="form-control form-control-md" maxLength="50" name="alarmmessage" placeholder="message" value={this.state.alarmmessage}
                                                    onChange={this.handleInputChange} type="text" />
                                            </div>
                                        </div>
                                    </MDBModalBody>
                                    <MDBModalFooter>
                                        <MDBBtn color="secondary" size="sm" onClick={this.toggle}>Close</MDBBtn>
                                        <MDBBtn color="primary" onClick={this.alarm} size="sm">Save</MDBBtn>
                                    </MDBModalFooter>
                                </MDBModal>
                            </MDBContainer>

                            <MDBContainer>

                                <MDBModal isOpen={this.state.alarmModal}>
                                    <MDBModalHeader toggle={this.alarmtoggle}>Alarm Message</MDBModalHeader>
                                    <MDBModalBody >
                                        <h3>{this.state.alarmmessage}</h3>
                                    </MDBModalBody>
                                    <MDBModalFooter>
                                        <MDBBtn color="secondary" size="sm" onClick={this.alarmtoggle}>Close</MDBBtn>
                                    </MDBModalFooter>
                                </MDBModal>
                            </MDBContainer>
                        </div>
                    </div>
                </div>
            </div>
        )
    }


}
export default Time;