import React from 'react';
import { MDBBtn } from "mdbreact";
import Rtif from '../../Rif';
import './StopWatch.css'

class StopWatch extends React.Component {
    constructor(props) {
        super()
        this.state = {
            milisec: 0,
            sec: 0,
            min: 0,
            hours: 0,
            startmil: 0,
            interval: '',
            startbtn: false,
            lapArray: []
        }
    }
    checkTime(i) {
        if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
        return i;
    }
    stopwatch(i) {

        let j = i++;
        j++;
        this.setState({ milisec: j++ })
        if (this.state.milisec > 99) {
            this.state.milisec = 0;
            this.setState({ sec: this.state.sec + 1 });
        }
        if (this.state.sec > 59) {
            this.state.sec = 0;
            this.setState({ min: this.state.min + 1 })
        }
        if (this.state.min > 59) {
            this.state.min = 0;
            this.setState({ hours: this.state.hours + 1 })
        }


    }
    start() {
        this.setState({
            interval: setInterval(() => {
                this.stopwatch(this.state.milisec)
            }, 10)
        })
        this.setState({ startbtn: true })

    }
    reset() {
        this.setState({ hours: 0, min: 0, sec: 0, milisec: 0,lapArray:[] })
    }
    stop() {
        clearInterval(this.state.interval)
        this.setState({ startbtn: false })
    }

    lap = () => {
        this.state.lapArray.push(
          {
              "lapTime":  this.checkTime(this.state.hours) + ':' + this.checkTime(this.state.min) + ':' + this.checkTime(this.state.sec) + ':' + this.checkTime(this.state.milisec),
              "totalTime":''
            }
        )
        console.log(this.state.lapArray)
    }

    componentDidMount() {

    }

    render() {
        // var startbtn = this.state.milisec > 0 ? false:true

        return (
            <div className="jumbtrons">


                <div className="col-md-4 offset-md-5">
                    <div className="col-md-4 offset-md-1">
                        <h6>Stop Watch</h6>

                    </div>
                    <h2>{this.checkTime(this.state.hours)}:{this.checkTime(this.state.min)}:{this.checkTime(this.state.sec)}:{this.checkTime(this.state.milisec)}</h2>

                </div>

                <div className="col-md-4 offset-md-5">
                    <div className="row">
                        <Rtif boolean={!this.state.startbtn}>
                            <div className="col-md-3">
                                <MDBBtn color="primary" size="sm" onClick={() => this.start()} >Start</MDBBtn>
                            </div>
                        </Rtif>
                        <Rtif boolean={!this.state.startbtn}>
                            <div className="col-md-3">
                                <MDBBtn size="sm" onClick={() => this.reset()}>Reset</MDBBtn>
                            </div>
                        </Rtif>
                        <Rtif boolean={this.state.startbtn}>
                            <div className="col-md-3">
                                <MDBBtn color="default" size="sm" onClick={this.lap}>Lap</MDBBtn>
                            </div>
                        </Rtif>
                        <Rtif boolean={this.state.startbtn}>
                            <div className="col-md-3">
                                <MDBBtn color="danger" size="sm" onClick={() => { this.stop() }}>Stop</MDBBtn>
                            </div>
                        </Rtif>
                    </div>
                </div>

                <div className="col-md-4 offset-md-5 lap">
                    <div className="row">
                        <div className="col-md-2">
                            <div>Lap</div>
                            <div>{this.state.lapArray.map((value,index)=>(
                                <div>{index+1}</div>
                            ))}</div>
                        </div>
                        <div className="col-md-4">
                            <div>Time</div>
                            <div>{this.state.lapArray.map((value,index)=>(
                                <div>{value.lapTime}</div>
                            ))}</div>
                        </div>
                        {/* <div className="col-md-4">
                            <div>Total Time</div>
                        </div> */}
                    </div>
                </div>
            </div>
        )
    }
}
export default StopWatch;