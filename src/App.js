import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './Routes';
import Topbar from './Components/TopbarComponent/Topbar';
import DarkTheme, { createTheme } from 'react-dark-theme';

function App() {

  const lightTheme = {
    background: 'white',
    text: 'black',
  }

  const darkTheme = {
    background: 'black',
    text: 'white',
  }
  const myTheme = createTheme(darkTheme, lightTheme)

  return (
    <div style={{ backgroundColor: myTheme.background, color: myTheme.text, minHeight: '100vh' }} >
      <Router>
        <Topbar />
        <div style={{marginTop:'50px'}}>
        <Routes />
        </div>
      </Router>

    </div>
  );
}

export default App;
